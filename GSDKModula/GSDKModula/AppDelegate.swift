//
//  AppDelegate.swift
//  GSDKModula
//
//  Created by KingYoung on 2020/05/27.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: DemoTableVC())
        window?.makeKeyAndVisible()
        
        return true
    }

  


}

