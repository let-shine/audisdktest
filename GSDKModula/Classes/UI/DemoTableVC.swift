//
//  DemoTableVC.swift
//  GSDKDemo
//
//  Created by KingYoung on 2020/05/8.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit
import MBBConnector


public class DemoTableVC: UITableViewController {

    let tableData = Services.allCases
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CellId")
      
    }

    // MARK: - Table view data source

    public override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tableData.count
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath)

        cell.textLabel?.text = tableData[indexPath.row].rawValue

        return cell
    }

    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
//        guard let _ = vehicle else {
//            let alert = UIAlertController(title: "Error", message: "先登录", preferredStyle: .alert)
//            let action = UIAlertAction(title: "确定", style: .default, handler: nil)
//            alert.addAction(action)
//            self.present(alert, animated: true, completion: nil)
//            return
//        }
        
        let alert = UIAlertController(title: "loading", message: "", preferredStyle: .alert)
        
        let service = tableData[indexPath.row]
        switch service {
        case .MBBLogin:
            present(alert, animated: true)
            self.login()
        case .MBBLogout:
            present(alert, animated: true)
            self.logout()
//        case .CarFinder:
            
//        case .RemoteHonkAndFlash:
//            self.remoteHonkAndFlash()
//        case .RemoteLock:
//            self.remoteLock()
//        case .RemoteUnlock:
//            self.remoteUnlock()
        default:
           navigationController?.pushViewController(DetailTableVC(server: service), animated: true)
        }
        
        
        
    }
   
    func login() {
        ADMediator.request(withKey: Req.GSDK.login, userInfo: nil) { (result) in
            self.dismiss(animated: true) {
                self.showResult(result)
            }
        }
    }
    
    func logout() {
           
    }

    
    func statusReport() {
        
        
        
    }
    
    func remoteHonkAndFlash() {
        
        
        
    }
    
    func remoteLock() {
        
        
    }
    
    func remoteUnlock() {
        
        
    }
    
    func remoteHeating() {
        
        
        
    }
    
    func showResult(_ result:Result<Any, RespError>) {
        var title = ""
        var msg = ""
        
        switch result {
        case let .success(message):
            title = "Success"
            msg = message as? String ?? "Success"
        case let .failure(error):
            title = "error"
            msg = error.msg
        default:
            break
        }
        
        dismiss(animated: true) {
            let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            let action = UIAlertAction(title: "确定", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    

}
