//
//  DetailTableVC.swift
//  GSDKDemo
//
//  Created by KingYoung on 2020/05/20.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit

class DetailTableVC: UITableViewController {
    let operations = ["123":[123,345],
                      "456":[23223, 232]]
    var serverType: Services?
    
    var dataSource: [Any] = []
    
    convenience init(server:Services) {
        self.init()
        serverType = server
        switch server {
        case .CarFinder:
            dataSource = Services.CarFinderService.allCases
        case .RemoteHonkAndFlash:
            dataSource = Services.RemoteHonkAndFlashService.allCases
        case .VehicleStatusReport:
            dataSource = Services.VehicleStatusReportService.allCases
        case .RemoteLockUnlock:
            dataSource = Services.RemoteHeatingService.allCases
        case .RemoteHeating:
            dataSource = Services.RemoteHeatingService.allCases
        case .ValetAlert:
            dataSource = Services.ValetAlertService.allCases
        case .SpeedAlert:
            dataSource = Services.SpeedAlertService.allCases
        case .Geofencing:
            dataSource = Services.GeofencingService.allCases
        case .RemoteBatteryCharge:
            dataSource = Services.RemoteBatteryChargeService.allCases
        case .RemotePretripClimatisation:
            dataSource = Services.RemotePretripClimatisationService.allCases
        case .RemoteDepartureTime:
            dataSource = Services.RemoteDepartureTimeService.allCases
        case .RemoteTripStatistics:
            dataSource = Services.RemoteTripStatisticsService.allCases
        case .DestinationImport:
             dataSource = Services.DestinationImportService.allCases
        case .TheftAlarm:
            dataSource = Services.TheftAlarmService.allCases
        case .VehicleTracking:
            dataSource = Services.VehicleTrackingService.allCases
        case .AssistanceCall:
            dataSource = Services.AssistanceCallService.allCases
        case .ServiceAppointment:
            dataSource = Services.ServiceAppointmentService.allCases
        case .RemoteProfileTimer:
            dataSource = Services.RemoteProfileTimerService.allCases
        case .ETronRoutePlanning:
            dataSource = Services.ETronRoutePlanningService.allCases
        case .PlugAndCharge:
            dataSource = Services.PlugAndChargeService.allCases
        default:
            break
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CellId")
     
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath)
        
        cell.textLabel?.text = "\(dataSource[indexPath.row])"
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let alert = UIAlertController(title: "loading", message: "", preferredStyle: .alert)
        present(alert, animated: true)
        
        ADMediator.request(withKey: dataSource[indexPath.row] as! AnyHashable, userInfo: nil) { (result) in
            self.dismiss(animated: true) {
                self.showResult(result)
            }
        }
    }
    
    func showResult(_ result:Result<Any, RespError>) {
        var title = ""
        var msg = ""
        
        switch result {
        case let .success(message):
            title = "Success"
            msg = message as? String ?? "Success"
        case let .failure(error):
            title = "error"
            msg = error.msg
        default:
            break
        }
        
        dismiss(animated: true) {
            let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            let action = UIAlertAction(title: "确定", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
}



