// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.1.3 (swiftlang-1100.0.282.1 clang-1100.0.33.15)
// swift-module-flags: -target arm64-apple-ios12.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name Observing
import Foundation
import Swift
import Utility
infix operator <~ : DefaultPrecedence
public class Bindable<ValueType> {
  public var value: ValueType {
    get
  }
  public init(_ initialValue: ValueType)
  @objc deinit
}
final public class ProtectedBindable<ValueType> : Observing.Bindable<ValueType> {
  public init(_ initialValue: ValueType, valueSetterHandler: (Observing.ProtectedBindable<ValueType>.Setter) -> Swift.Void)
  convenience public init(_ initialValue: ValueType, valueBinderHandler: (Observing.Binder<ValueType>) -> Swift.Void)
  public typealias ValueSetterHandler = (Observing.ProtectedBindable<ValueType>.Setter) -> Swift.Void
  public typealias ValueBinderHandler = (Observing.Binder<ValueType>) -> Swift.Void
  public struct Setter {
    public func set(_ value: ValueType)
  }
  public static func bindable(initialValue: ValueType) -> (bindable: Observing.ProtectedBindable<ValueType>, setter: Observing.ProtectedBindable<ValueType>.Setter)
  public static func bindableWithBinder(initialValue: ValueType) -> (bindable: Observing.ProtectedBindable<ValueType>, binder: Observing.Binder<ValueType>)
  override public init(_ initialValue: ValueType)
  @objc deinit
}
@propertyWrapper final public class MutableBindable<ValueType> : Observing.Bindable<ValueType> {
  final public var wrappedValue: ValueType {
    get
    set
  }
  override final public var value: ValueType {
    get
    set
  }
  final public var projectedValue: Observing.MutableBindable<ValueType> {
    get
  }
  convenience public init(wrappedValue: ValueType)
  final public var valueBinder: Observing.Binder<ValueType> {
    get
  }
  override public init(_ initialValue: ValueType)
  @objc deinit
}
extension Bindable {
  public func map<Transformed>(_ transform: @escaping (ValueType) -> Transformed) -> Observing.Bindable<Transformed>
  public func flatMap<Transformed>(_ transform: @escaping (ValueType) -> Observing.Bindable<Transformed>) -> Observing.Bindable<Transformed>
}
extension MutableBindable {
  final public func bindable() -> Observing.Bindable<ValueType>
}
final public class Binder<ValueType> {
  final public func bind(to bindable: Observing.Bindable<ValueType>)
  final public func bind<FromType>(to bindable: Observing.Bindable<FromType>, via transformer: @escaping (FromType) -> ValueType)
  final public func bind<FromType1, FromType2>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, via transformer: @escaping (FromType1, FromType2) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, via transformer: @escaping (FromType1, FromType2, FromType3) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, _ bindable10: Observing.Bindable<FromType10>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, _ bindable10: Observing.Bindable<FromType10>, _ bindable11: Observing.Bindable<FromType11>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, _ bindable10: Observing.Bindable<FromType10>, _ bindable11: Observing.Bindable<FromType11>, _ bindable12: Observing.Bindable<FromType12>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, _ bindable10: Observing.Bindable<FromType10>, _ bindable11: Observing.Bindable<FromType11>, _ bindable12: Observing.Bindable<FromType12>, _ bindable13: Observing.Bindable<FromType13>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, _ bindable10: Observing.Bindable<FromType10>, _ bindable11: Observing.Bindable<FromType11>, _ bindable12: Observing.Bindable<FromType12>, _ bindable13: Observing.Bindable<FromType13>, _ bindable14: Observing.Bindable<FromType14>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, _ bindable10: Observing.Bindable<FromType10>, _ bindable11: Observing.Bindable<FromType11>, _ bindable12: Observing.Bindable<FromType12>, _ bindable13: Observing.Bindable<FromType13>, _ bindable14: Observing.Bindable<FromType14>, _ bindable15: Observing.Bindable<FromType15>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15, FromType16>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, _ bindable10: Observing.Bindable<FromType10>, _ bindable11: Observing.Bindable<FromType11>, _ bindable12: Observing.Bindable<FromType12>, _ bindable13: Observing.Bindable<FromType13>, _ bindable14: Observing.Bindable<FromType14>, _ bindable15: Observing.Bindable<FromType15>, _ bindable16: Observing.Bindable<FromType16>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15, FromType16) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15, FromType16, FromType17>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, _ bindable10: Observing.Bindable<FromType10>, _ bindable11: Observing.Bindable<FromType11>, _ bindable12: Observing.Bindable<FromType12>, _ bindable13: Observing.Bindable<FromType13>, _ bindable14: Observing.Bindable<FromType14>, _ bindable15: Observing.Bindable<FromType15>, _ bindable16: Observing.Bindable<FromType16>, _ bindable17: Observing.Bindable<FromType17>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15, FromType16, FromType17) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15, FromType16, FromType17, FromType18>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, _ bindable10: Observing.Bindable<FromType10>, _ bindable11: Observing.Bindable<FromType11>, _ bindable12: Observing.Bindable<FromType12>, _ bindable13: Observing.Bindable<FromType13>, _ bindable14: Observing.Bindable<FromType14>, _ bindable15: Observing.Bindable<FromType15>, _ bindable16: Observing.Bindable<FromType16>, _ bindable17: Observing.Bindable<FromType17>, _ bindable18: Observing.Bindable<FromType18>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15, FromType16, FromType17, FromType18) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15, FromType16, FromType17, FromType18, FromType19>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, _ bindable10: Observing.Bindable<FromType10>, _ bindable11: Observing.Bindable<FromType11>, _ bindable12: Observing.Bindable<FromType12>, _ bindable13: Observing.Bindable<FromType13>, _ bindable14: Observing.Bindable<FromType14>, _ bindable15: Observing.Bindable<FromType15>, _ bindable16: Observing.Bindable<FromType16>, _ bindable17: Observing.Bindable<FromType17>, _ bindable18: Observing.Bindable<FromType18>, _ bindable19: Observing.Bindable<FromType19>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15, FromType16, FromType17, FromType18, FromType19) -> ValueType)
  final public func bind<FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15, FromType16, FromType17, FromType18, FromType19, FromType20>(to bindable1: Observing.Bindable<FromType1>, _ bindable2: Observing.Bindable<FromType2>, _ bindable3: Observing.Bindable<FromType3>, _ bindable4: Observing.Bindable<FromType4>, _ bindable5: Observing.Bindable<FromType5>, _ bindable6: Observing.Bindable<FromType6>, _ bindable7: Observing.Bindable<FromType7>, _ bindable8: Observing.Bindable<FromType8>, _ bindable9: Observing.Bindable<FromType9>, _ bindable10: Observing.Bindable<FromType10>, _ bindable11: Observing.Bindable<FromType11>, _ bindable12: Observing.Bindable<FromType12>, _ bindable13: Observing.Bindable<FromType13>, _ bindable14: Observing.Bindable<FromType14>, _ bindable15: Observing.Bindable<FromType15>, _ bindable16: Observing.Bindable<FromType16>, _ bindable17: Observing.Bindable<FromType17>, _ bindable18: Observing.Bindable<FromType18>, _ bindable19: Observing.Bindable<FromType19>, _ bindable20: Observing.Bindable<FromType20>, via transformer: @escaping (FromType1, FromType2, FromType3, FromType4, FromType5, FromType6, FromType7, FromType8, FromType9, FromType10, FromType11, FromType12, FromType13, FromType14, FromType15, FromType16, FromType17, FromType18, FromType19, FromType20) -> ValueType)
  final public func bind<SomeSequence, FromType>(to sequence: SomeSequence, via transformer: @escaping ([FromType]) -> ValueType) where SomeSequence : Swift.Sequence, SomeSequence.Element == Observing.Bindable<FromType>
  final public func unbind()
  public init(valueDidChange: @escaping (ValueType) -> Swift.Void)
  @objc deinit
}
public func <~ <ValueType>(binder: Observing.Binder<ValueType>, bindable: Observing.Bindable<ValueType>)
public func <~ <ValueType>(binder: Observing.Binder<ValueType?>, bindable: Observing.Bindable<ValueType>)
public func <~ <ValueType, ErrorType>(binder: Observing.Binder<ValueType?>, loadable: Observing.Loadable<ValueType, ErrorType>) where ErrorType : Swift.Error
public func <~ <ValueType, ErrorType>(binder: Observing.Binder<ValueType?>, loadable: Observing.Loadable<ValueType?, ErrorType>) where ErrorType : Swift.Error
public func <~ <ValueType, ErrorType>(binder: Observing.Binder<ErrorType?>, loadable: Observing.Loadable<ValueType, ErrorType>) where ErrorType : Swift.Error
extension Loadable {
  public func reload(completion: @escaping () -> Swift.Void)
  public func loadIfNotYetLoaded(completion: @escaping Observing.Loadable<ValueType, ErrorType>.CompletionHandler)
  public func loadIfStale(staleAfter timeInterval: Foundation.TimeInterval, completion: @escaping Observing.Loadable<ValueType, ErrorType>.CompletionHandler)
}
public protocol CancellableError : Swift.Error {
  static func cancelledError() -> Self
}
extension Loadable.Setter where ErrorType : Observing.CancellableError {
  public func cancel()
  public func reset()
}
final public class TypedSignalEmitter<SignalType> {
  final public func emit(_ value: SignalType)
  public init()
  @objc deinit
}
final public class TypedSignalReceiver<SignalType> {
  public typealias SignalHandler = (SignalType) -> Swift.Void
  public init(_ handler: @escaping Observing.TypedSignalReceiver<SignalType>.SignalHandler)
  final public func bind(to emitter: Observing.TypedSignalEmitter<SignalType>, queue: Dispatch.DispatchQueue? = nil)
  final public func bind(to emitters: [Observing.TypedSignalEmitter<SignalType>], queue: Dispatch.DispatchQueue? = nil)
  final public func unbind()
  @objc deinit
}
public func <~ <T>(receiver: Observing.TypedSignalReceiver<T>, emitter: Observing.TypedSignalEmitter<T>)
public func <~ <T>(receiver: Observing.TypedSignalReceiver<T>, emitters: [Observing.TypedSignalEmitter<T>])
public typealias SignalEmitter = Observing.TypedSignalEmitter<Swift.Void>
public typealias SignalReceiver = Observing.TypedSignalReceiver<Swift.Void>
extension TypedSignalEmitter where SignalType == Swift.Void {
  final public func emit()
}
open class Loadable<ValueType, ErrorType> where ErrorType : Swift.Error {
  public typealias CompletionHandler = (Swift.Result<ValueType, ErrorType>) -> Swift.Void
  public typealias LoadingHandler = (@escaping Observing.Loadable<ValueType, ErrorType>.CompletionHandler) -> Swift.Void
  final public let stateBindable: Observing.ProtectedBindable<Observing.LoadingState<ErrorType>>
  final public let resultBindable: Observing.ProtectedBindable<Observing.LoadableResult<ValueType>?>
  public init(result: Observing.LoadableResult<ValueType>? = nil, valueSetterHandler: Observing.Loadable<ValueType, ErrorType>.ValueSetterHandler? = nil, loadingHandler: @escaping Observing.Loadable<ValueType, ErrorType>.LoadingHandler)
  public typealias ValueSetterHandler = (Observing.Loadable<ValueType, ErrorType>.Setter) -> Swift.Void
  public struct Setter {
    public typealias Implementation = Observing.Loadable<ValueType, ErrorType>
    public func set(_ value: ValueType)
  }
  @objc deinit
}
extension Loadable {
  public static func loadable(with result: Observing.LoadableResult<ValueType>? = nil, loadingHandler: @escaping Observing.Loadable<ValueType, ErrorType>.LoadingHandler) -> (Observing.Loadable<ValueType, ErrorType>, Observing.Loadable<ValueType, ErrorType>.Setter)
}
public protocol LoadableObserver : AnyObject {
  func loadableDidChange()
}
@frozen public enum LoadingState<Error> {
  case notYetLoaded
  case loading
  case loaded
  case error(Error)
}
extension LoadingState {
  public var isLoading: Swift.Bool {
    get
  }
  public var error: Error? {
    get
  }
}
extension Loadable {
  public var error: ErrorType? {
    get
  }
  public var lastUpdated: Foundation.Date? {
    get
  }
  public var isLoading: Swift.Bool {
    get
  }
  public var state: Observing.LoadingState<ErrorType> {
    get
  }
  public var result: Observing.LoadableResult<ValueType>? {
    get
  }
  public func reload(completion: ((Swift.Result<ValueType, ErrorType>) -> Swift.Void)? = nil)
  public func add(observer: Observing.LoadableObserver)
  public func remove(observer: Observing.LoadableObserver)
}
public class Observers<Type> : Swift.Sequence {
  public init()
  public func removeAll()
  public func makeIterator() -> Observing.Observers<Type>.Iterator
  public var count: Swift.Int {
    get
  }
  public func add(_ observer: Type)
  public func remove(_ observer: Type)
  public typealias Element = Type
  @objc deinit
}
extension Observers {
  public var isEmpty: Swift.Bool {
    get
  }
}
extension Observers {
  public struct Iterator : Swift.IteratorProtocol {
    public typealias Element = Type
    mutating public func next() -> Observing.Observers<Type>.Iterator.Element?
  }
}
public struct LoadableResult<ValueType> {
  public let value: ValueType
  public let date: Foundation.Date
  public init(value: ValueType, date: Foundation.Date)
}
extension LoadableResult : Utility.Serializable, Swift.Encodable where ValueType : Swift.Encodable {
  public func serialized() -> Utility.Serialized
}
extension LoadableResult : Utility.Deserializable, Swift.Decodable where ValueType : Swift.Decodable {
  public init(serialized: Utility.Serialized) throws
}
extension LoadableResult : Utility.ContextDeserializable where ValueType : Utility.ContextDeserializable {
  public typealias Context = ValueType.Context
  public init(serialized: Utility.Serialized, context: ValueType.Context) throws
}
