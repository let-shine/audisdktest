
@import Foundation;

NS_ASSUME_NONNULL_BEGIN

@class AAFLicense;

@protocol AAFLicenseProvider<NSObject>

+ (AAFLicense *)license;

@end

/**
 License Object
 */
@interface AAFLicense : NSObject

@property (nonatomic, copy, readonly) NSString *frameworkName;   /* Framework name */
@property (nonatomic, copy, readonly, nullable) NSString *title; /* License title */
@property (nonatomic, copy, readonly) NSString *body;            /* License body */

/**
 Use this method to create a license object. A license consists of a framework title and copyright information
 @param  frameworkName Framework title
 @param  title License Title (title)
 @param  body License body
 */
- (instancetype)initWithFrameworkName:(NSString *)frameworkName
                                title:(nullable NSString *)title
                                 body:(NSString *)body;

/**
 Use this method if your framework bundle contains a LICENSE.txt file.
 
 Example:
 AAFLicense *exampleLicense = [[AAFLicense alloc] initWithFrameworkName:@"ExampleFrameworkTitle"
                                            bundleContainingLicenseFile:[NSBundle bundleForClass:[self class]]];
 
 @param  frameworkName Framework title
 @param  bundle Pass the bunde
 */
- (instancetype)initWithFrameworkName:(NSString *)frameworkName
          bundleContainingLicenseFile:(NSBundle *)bundle;

/**
 Use this method if your framework bundle contains a license file with any name.
 
 @param  frameworkName Framework title
 @param  path Path to the license file
 */
- (instancetype)initWithFrameworkName:(NSString *)frameworkName
                   andLicenseFilePath:(NSString *)path;

@end
NS_ASSUME_NONNULL_END
