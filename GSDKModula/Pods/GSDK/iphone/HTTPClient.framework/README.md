# HTTPClient

`HTTPClient` serves as the underlying framework for other frameworks. There are two use cases you can encounter:

1. Initializing a `Client`instance for another framework.
2. Using `HTTPClient` to implement a REST API.


## Configuring `Client` for usage with another framework.

Other frameworks may require an instance of `Client` for initialization. This is the `Client` object that is used by the framework internally to perform HTTP requests.
To initialize a `Client` object, you need a manager object conforming to the `RequestManager` protocol.
`HTTPClient` already ships with two implementations of the `RequestManager` protocol: 

1. `URLSessionRequestManager`: A `RequestManager` implementation that uses iOS' URLSession class.
2. `MockRequestManager`: A `RequestManager` implementation that can be used for testing and mocking.


### Configuring `URLSessionRequestManager`

`URLSessionRequestManager` accepts two parameters: 

1. policies: A dictionary to specify TLS pinning or client certificate authentication
2. options: A set of options, e.g. to only allow HTTPS connections.

Configuring Root Certificate evaluation for the host *example.com* with the Root Certificate SHA256 fingerprint `0836fe9394c5196a9b94757deee97da3a5c4752ce444c49d3b8e747fd54d2a22`

```swift
// Initialize URLSessionManager
let urlSessionManager = URLSessionManager(policies: ["example.com" : .verifyRoot([.sha256Fingerprint("0836fe9394c5196a9b94757deee97da3a5c4752ce444c49d3b8e747fd54d2a22")])])

// Initialize Client
let client = Client(requestManager: urlSessionManager)

```

## Using `HTTPClient` to implement a REST API

HTTPClient follows the Request/Response pattern to implement REST APIs. 

For every endpoint you want to connect to, you implement a struct/class that conforms to the `Request` protocol. 
Every request also has an associated `ResultPayloadType` that respresents the expected payload parsed from the server response. 
For `JSON` payloads or empty responses, HTTPClient already includes methods and extensions to simplify handling these types of responses.
Other parsing or handling of responses can be implemented by implementing the `Request.resultPayload(from:)`  method in the `Request` struct/class.


### Example endpoint

- **Server**: `https://jsonplaceholder.typicode.com`
- **Endpoint:** `GET /post/<id>` 
- **Status code:** `200`
- **Body description:**
```
{
    "id":<Int>, 
    "userId":<Int>", 
    "title":<String>, 
    "body": <String>
}
```

### Implementing a `Request`

Let's implement a request that loads a post from the endpoint above:

A post is represented by the `Post` struct in our application:

```swift

struct Post {
    let id: Int
    let userId: Int
    let title: String
    let body: String
}
```

We also have an error type in our application which is currently empty:

```swift

enum ApplicationError {

}
```

To load a post, we first declare a new struct representing the request that conforms to the `Request` protocol
```swift

struct PostRequest: Request {

}
```

Now we have to implement all the required properties and methods of the protocol (there are some default implementations, here we try to do everything on our own to understand what's going on): 

```swift

struct PostRequest: Request {
    
    typealias ResultPayloadType = Post // We define the type of the parsed payload
    typealias ErrorPayloadType = JSONServerError // A generic error object for error with a JSON payload.
    typealias ConnectorErrorType = ApplicationError // A error type representing application level errors
    
    var acceptableStatusCodes: [StatusCode] { // The HTTP status codes we expect for a (successful) response from the endpoint
        return [.ok]
    }
    
    let postId: Int // a property to hold the Id of the post we want to load
    
    func request() -> HTTPRequest { // This method creates a HTTPRequest instance
        
        let baseURL = URL(string: "https://jsonplaceholder.typicode.com")!
        
        return HTTPRequest(.get, baseURL: baseURL, endpoint: "posts/\(postId)")
    }
    
    // resultPayload(from:) is called a when request was successfully performed and the loaded data has to be parsed.
    // The method below is also the default implementation inside HTTPClient, so if your ResultPayloadType conforms to JSONDecodable, 
    // you can omit this method.
    func resultPayload(from response: HTTPResponse) throws -> Post {
        
        let json = try response.jsonBody()
        let post = try Post(json: json)
        return post
    }
    
    // This method creates a JSONServerError (ErrorPayloadType) for a failed response 
    func errorPayload(from response: HTTPResponse) throws -> JSONServerError {
        return try JSONServerError(response: response)
    }
    
    // This method is used to map from an ErrorPayloadType to a ConnectorErrorType.
    // For more information, see Error Handling below
    func error(from errorPayload: JSONServerError) -> ConnectorErrorType? {
        return nil
    }
}
```

To run the code above, we still have to implement a couple of two things:

1. `Post` has to conform to the `JSONDecodable` protocol. (see the section on JSON Parsing below)
2. `ApplicationError` has to conform to the `ConnectorError` protocol. 


Let's start with `Post` and `JSONDecodable` by writing an extension:

``` swift

extension Post: JSONDecodable {
    
    init(json: JSON) throws {
    
        self.init(id: try json.decode(at: "id"), 
                  userId: try json.decode(at: "userId"),
                  title: try json.decode(at: "title"),
                  body: try json.decode(at: "body"))
    }
}
```
Now we modify `ApplicationError` to conform to `ConnectorError`:

```swift

enum ApplicationError: ConnectorError {

    case genericHTTPError(HTTPError)

    static func error(from httpError: HTTPError, for requestIdentifier: UniqueRequestIdentifier) -> ApplicationError {

        return .genericHTTPError(httpError)
    }
}
```
Now we should be able to perform a request. 

#### Other HTTP methods

While the example covers only `GET`, the methods are identical for other HTTP methods (e.g. just pass `.post` instead of `.get` when creating the `HTTPRequest` in `request()` in your request struct). 
The `HTTPRequest` initializer accepts a variety of different arguments to configure  headers, query parameters, a body, supported encodings, or supported content types. 

### Performing a request

To perform a request, you need a client instance

```swift
//init client with default request manager:
client = Client(requestManager: URLSessionRequestManager())
```
Now you can send a request . 

```swift

let postRequest = PostRequest(messageId: 15)

client.performRequest(postRequest) { (result) in

    switch result {
    case .success(let post: Post):
        print(message)
        
    case .failure(let error: Error):
        print(error)
    }
}
```

### Error Handling

Every `Request` implementation has two associated error types:

1. ErrorPayloadType: A type representing a error response from the server. HTTPClient already includes two predefined types, `JSONServerError` and `OAuthServerError` that can be used for this type.
2. ConnectorErrorType: This is an error type in the application's context.

Let's see how we would use `JSONServerError` and an application's error type to  map error responses of the server:
For example, we assume the  `GET /post/<id>`  endpoint would require some kind of authorization. If it is missing, the server responds with a `401 Unauthorized` response with the JSON body`{"error": 1000}`.
We want to map this error to our `ApplicationError.userNotLoggedIn` error:

```swift

enum ApplicationError: ConnectorError {

    case userNotLoggedIn
    case genericHTTPError(HTTPError)
    
    static func error(from httpError: HTTPError, for requestIdentifier: UniqueRequestIdentifier) -> ApplicationError {
        
        return .genericHTTPError(httpError)
    }
}

...

// Implementation of func error(from errorPayload: ErrorPayloadType) -> ConnectorErrorType? in your Request implementation:

struct PostRequest: Request {

    typealias ErrorPayloadType = JSONServerError
    typealias ConnectorErrorType = ApplicationError

...

    func error(from errorPayload: JSONServerError) -> ApplicationError? {
    
        guard errorPayload.statusCode == .unauthorized else {
            return nil
        }
        
        do {
            let errorCode = errorPayload.json.getInt(at: "error")
            if errorCode == 1000 {
                return .userNotLoggedIn
            }
        } catch {
           
        }
        
        return nil
    }

...

}
```

## Providing Authorization for Requests

Like already mentioned above, some HTTP requests require some kind of authorization, for example a access token in the `Authorization` header of a request.
To implement this, your request implementation may conform to the `Authorized` protocol in addition to the `Request` protocol or the `AuthorizedRequest`, which is just a combination of `Request` and `Authorized`.

`Authorized` only requires one additional property of type `AuthorizationProvider`: 
```swift
    var authorizationProvider: AuthorizationProvider
```

`AuthorizationProvider` is also a protocol with one method:

```swift
    func authorization(completion: @escaping (AuthorizationProviderResult) -> Swift.Void)
```

Use this method to obtain and return the authorization for a request. The authorization can be returned as dictionary of headers (`Authorization.headers([Header : String]`) or parameters (`Authorization.params([String : String])`)

To pass the `Authorization` to your request, you have to change the `request()`  method in your request for one of the following:

- `func request(with authorizationHeaders: [Header : String]) throws -> HTTPRequest`: If you expect authorization headers
- `func request(with authorizationParams: [String : String]) throws -> HTTPRequest`: If you expect authorization params

## App Transport Security (ATS)

Apple introduced a security feature called App Transport Security (ATS) in iOS 9.0.
ATS enforces the usage of TLS 1.2 and Perfect Forward Secrecy (PFS) in the default configuration. 
If a host you try to connect to does not meet these requirements, the higher level networking API fails with an error that looks like the following one:

```
Error Domain=NSURLErrorDomain Code=-1200 "An SSL error has occurred and a secure connection to the server cannot be made." UserInfo={NSErrorFailingURLStringKey=<failing URL>, NSLocalizedRecoverySuggestion=Would you like to connect to the server anyway?, _kCFStreamErrorDomainKey=3, _NSURLErrorFailingURLSessionTaskErrorKey=LocalDataTask <A4737A5F-CAAF-4FF2-83F0-D9B38927BB61>.<10>, _NSURLErrorRelatedURLSessionTaskErrorKey=(
"LocalDataTask <A4737A5F-CAAF-4FF2-83F0-D9B38927BB61>.<10>"), NSLocalizedDescription=An SSL error has occurred and a secure connection to the server cannot be made., NSErrorFailingURLKey=<failing URL>, NSUnderlyingError=0x600000b541b0 {Error Domain=kCFErrorDomainCFNetwork Code=-1200 "(null)" UserInfo={_kCFStreamPropertySSLClientCertificateState=0, _kCFNetworkCFStreamSSLErrorOriginalValue=-9824, _kCFStreamErrorDomainKey=3, _kCFStreamErrorCodeKey=-9824}}, _kCFStreamErrorCodeKey=-9824} [-1200]
```

ATS supports exceptions to let you connecto to a host that does not meet these requirements. The exceptions are configured using an applications `Info.plist` file.
For more information, see the [ATS documentation](https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/CocoaKeys.html#//apple_ref/doc/uid/TP40009251-SW33)
